var Scanner = require('com.noizeramp.nrscanner');

// open a single window
var win = Ti.UI.createWindow({ backgroundColor: 'white', layout: 'vertical' });
var typeLabel = Ti.UI.createLabel();
var codeLabel = Ti.UI.createLabel();
win.add(typeLabel);
win.add(codeLabel);

var barcodeBtn = Ti.UI.createButton({ title: 'Barcode' });
barcodeBtn.addEventListener('click', function(e) {
  Scanner.scanBarcode({
    success: function(data) {
      typeLabel.text = data.type;
      codeLabel.text = data.code;
    }
  })  
});
win.add(barcodeBtn);


var qrcodeBtn = Ti.UI.createButton({ title: 'QR Code' });
qrcodeBtn.addEventListener('click', function(e) {
  Scanner.scanQRCode({
    success: function(data) {
      typeLabel.text = data.type;
      codeLabel.text = data.code;
    }
  })  
});
win.add(barcodeBtn);

win.open();