package com.noizeramp.nrscanner;

import com.google.zxing.Result;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import android.content.Intent;

public class BarcodeScannerActivity extends ScannerActivity implements ZXingScannerView.ResultHandler {
	
	protected void configureScannerView(ZXingScannerView view) {
		// standard barcode is default
	}

	protected void parseResult(Result rawResult) {
        Intent result = new Intent();
        result.putExtra("code", rawResult.getText());
        result.putExtra("type", rawResult.getBarcodeFormat().toString());
        setResult(0xCAFE, result);
	}
	
}