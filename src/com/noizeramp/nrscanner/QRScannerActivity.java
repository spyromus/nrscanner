package com.noizeramp.nrscanner;

import java.util.ArrayList;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import android.content.Intent;

public class QRScannerActivity extends ScannerActivity implements ZXingScannerView.ResultHandler {
	
	protected void configureScannerView(ZXingScannerView view) {
		ArrayList<BarcodeFormat> formats = new ArrayList<BarcodeFormat>();
		formats.add(BarcodeFormat.QR_CODE);
		view.setFormats(formats);
	}

	protected void parseResult(Result rawResult) {
        Intent result = new Intent();
        result.putExtra("code", rawResult.getText());
        result.putExtra("type", rawResult.getBarcodeFormat().toString());
        setResult(0xCAFE, result);
	}
	
}