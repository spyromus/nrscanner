package com.noizeramp.nrscanner;

import com.google.zxing.Result;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

public abstract class ScannerActivity extends Activity implements ZXingScannerView.ResultHandler {
	private ZXingScannerView mScannerView;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(
        	WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        mScannerView = new ZXingScannerView(this);
        configureScannerView(mScannerView);
        setContentView(mScannerView);
    }

	@Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
    	parseResult(rawResult);
        finish();
    }

    protected abstract void configureScannerView(ZXingScannerView view);
	protected abstract void parseResult(Result rawResult);
}
